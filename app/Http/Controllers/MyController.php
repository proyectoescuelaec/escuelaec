<?php

namespace EscuelaEC\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
	public function index(){
    	return view('welcome');
    }

    public function nombre($nombre = "name"){
    	return "Mi nombre es: ".$nombre;
    }

    public function admin(){
    	return view('admin');
    }

    public function cine(){
    	return view('cine');
    }

    public function videos(){
    	return view('videos');
    }

    public function reviews(){
    	return view('reviews');
    }

    public function error(){
    	return view('404');
    }

    public function contact(){
    	return view('contact');
    }
}
