<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('prueba/{nombre}', function($nombre){
	return "Hola, ".$nombre;
});
*/

Route::get('/','MyController@index');
Route::get('nombre/{nombre?}','MyController@nombre');
Route::get('admin','MyController@admin');
Route::get('cine','MyController@cine');
Route::get('videos','MyController@videos');
Route::get('reviews','MyController@reviews');
Route::get('404','MyController@error');
Route::get('contact','MyController@contact');